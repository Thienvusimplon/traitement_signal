from flask import Flask, render_template, request
import librosa
from IPython.display import Audio
import numpy as np
import pickle
import pandas as pd

app = Flask(__name__)

# generate dataset from the audio signal, the samplerate and the label
def generate_dataset(audio, sr, label):
    spec = librosa.feature.melspectrogram(y=audio, sr=sr)
    X = pd.DataFrame(spec.T)
    y = pd.Series(label).repeat(X.shape[0])
    return X, y


ls = ["tivi", "nolan", "armand"]


def generateArrayWithDfs(person):
    X = pd.DataFrame()
    y = pd.Series()
    for files in person:
        person_path = os.listdir(files + '/')
        for elm in person_path:
            person_sound, person_sr = librosa.load(os.path.join(os.getcwd(), files, elm))
            # extract non-silent intervals from the voice signal
            voice_intervals = librosa.effects.split(person_sound, top_db=50)
            voice_clean = np.zeros(person_sound.shape)
            # b, a = scipy.signal.iirfilter(2, Wn=[60.0, 4096.0], fs=person_sr, btype="bandpass")

            # apply the filter using `scipy.signal.lfilter`
            # y_voice_filt = scipy.signal.lfilter(b, a, person_sound)
            for start, end in voice_intervals:
                voice_clean[start:end] = person_sound[start:end]
            X_person, y_person = generate_dataset(voice_clean, person_sr, files)
            dfx = pd.DataFrame(X_person)
            dfy = pd.Series(y_person)
            X = pd.concat([X, dfx])
            y = pd.concat([y, dfy])

    return X, y


@app.route("/")
def hello_world():
    return render_template('index.html')

@app.route('/uploader', methods = ['POST'])
def upload_file():
    if request.method == 'POST':
        X = pd.DataFrame()
        y = pd.Series()

        f = request.files['file']
        person_sound, person_sr = librosa.load(f)
        # print("||||||||||||", y.shape, "|||||||||||", sr)
        voice_detection_model = pickle.load(open("model", 'rb'))

        # voice_intervals = librosa.effects.split(person_sound, top_db=50)
        # voice_clean = np.zeros(person_sound.shape)
        # # b, a = scipy.signal.iirfilter(2, Wn=[60.0, 4096.0], fs=person_sr, btype="bandpass")
        #
        # # apply the filter using `scipy.signal.lfilter`
        # # y_voice_filt = scipy.signal.lfilter(b, a, person_sound)
        # for start, end in voice_intervals:
        #     voice_clean[start:end] = person_sound[start:end]
        X_person, y_person = generate_dataset(person_sound, person_sr, "balek")
        dfx = pd.DataFrame(X_person)
        # dfy = pd.Series(y_person)
        X = pd.concat([X, dfx])
        # y = pd.concat([y, dfy])

        y_pred = voice_detection_model.predict(X)

        pred_unique_labels, pred_unique_counts = np.unique(y_pred, return_counts=True)
        pred_unique_labels, pred_unique_counts
        return render_template("zdraaa.html", unique_labels = pred_unique_labels, unique_counts=pred_unique_counts)
        return f"<table class='table'><thead><tr><th>{pred_unique_labels[0]}</th><th>{pred_unique_labels[1]}</th><th>{pred_unique_labels[-1]}</th></thead></tr><tr><th>{pred_unique_counts[0]} </th><th>{pred_unique_counts[1]} </th><th>{pred_unique_counts[2]} </th></tr></table>"